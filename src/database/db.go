package database

import (
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"www.lama.com/src/models"
)

var DB *gorm.DB

func Connect() {
	var err error
	dsn := "root:root@tcp(db:3306)/test?charset=utf8mb4&parseTime=True&loc=Local"
	DB, err = gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		panic("could not connect with database")
	}

}

func AutoMigrate() {
	DB.AutoMigrate(models.User{}, models.Product{}, models.Link{}, models.Order{}, models.OrderItem{})
}

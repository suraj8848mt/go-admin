package routes

import (
	"github.com/gofiber/fiber/v2"
	"www.lama.com/src/controllers"
	"www.lama.com/src/middlewares"
)

func Setup(app *fiber.App) {
	// routes prefix
	api := app.Group("api")

	// admin routes
	admin := api.Group("admin")
	admin.Post("register", controllers.Register)
	admin.Post("login", controllers.Login)

	adminAuthenticated := admin.Use(middlewares.IsAuthenticate)
	adminAuthenticated.Get("user", controllers.IsAuthenticatedUser)
	adminAuthenticated.Post("logout", controllers.Logout)
	adminAuthenticated.Put("user/info", controllers.UpdateInfo)
	adminAuthenticated.Put("user/password", controllers.UpdatePassword)
	adminAuthenticated.Get("ambassadors", controllers.Ambassadors)
	adminAuthenticated.Get("products", controllers.Products)
	adminAuthenticated.Post("product", controllers.CreateProducts)
	adminAuthenticated.Get("product/:id", controllers.GetProductById)
	adminAuthenticated.Put("product/:id", controllers.UpdateProduct)
	adminAuthenticated.Delete("product/:id", controllers.DeleteProduct)
	adminAuthenticated.Get("users/:id/links", controllers.Link)
	adminAuthenticated.Get("orders", controllers.Orders)

	// AMBASSADOR routes
	ambassador := api.Group("ambassador")
	ambassador.Post("register", controllers.Register)
	ambassador.Post("login", controllers.Login)

	ambassadorAuthenticated := ambassador.Use(middlewares.IsAuthenticate)
	ambassadorAuthenticated.Get("user", controllers.IsAuthenticatedUser)
	ambassadorAuthenticated.Post("logout", controllers.Logout)
	ambassadorAuthenticated.Put("user/info", controllers.UpdateInfo)
	ambassadorAuthenticated.Put("user/password", controllers.UpdatePassword)

}

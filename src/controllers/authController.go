package controllers

import (
	"strconv"
	"strings"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/golang-jwt/jwt"
	"www.lama.com/src/database"
	"www.lama.com/src/middlewares"
	"www.lama.com/src/models"
	"www.lama.com/src/validator"
)

//** Register function
func Register(c *fiber.Ctx) error {
	var data map[string]string

	if err := c.BodyParser(&data); err != nil {
		return err
	}
	// compare password and confirm password if not equal return error
	if data["password"] != data["password_confirm"] {
		return c.Status(400).JSON(fiber.Map{
			"message": "Password and password confirmation do not match",
		})
	}

	user := &models.User{
		FirstName:    data["first_name"],
		LastName:     data["last_name"],
		Email:        data["email"],
		IsAmbassador: strings.Contains(c.Path(), "/api/ambassador"),
	}

	// set password to bcrypt hash
	user.Password = user.SetPassword(data["password"])

	// Initialize a new validator instance and use it to validate the input.
	v := validator.New()

	// Validate the input.
	if models.ValidateUser(v, user); !v.Valid() {
		return c.Status(400).JSON(v.Errors)
	}

	// create user
	database.DB.Create(&user)

	return c.JSON(user)
}

//** Login function
func Login(c *fiber.Ctx) error {
	var data map[string]string

	if err := c.BodyParser(&data); err != nil {
		return err
	}

	user := models.User{}

	// find user by email
	database.DB.Where("email = ?", data["email"]).First(&user)

	// see if user exists
	if user.Id == 0 {
		c.Status(fiber.StatusBadRequest)
		return c.JSON(fiber.Map{
			"message": "User not found",
		})
	}

	// compare password with bcrypt hash
	if err := user.ComparePassword(data["password"]); err != nil {
		return c.Status(400).JSON(fiber.Map{
			"message": "invalid credentials",
		})
	}

	// create jwt token
	payload := jwt.StandardClaims{
		ExpiresAt: time.Now().Add(time.Hour * 24).Unix(),
		Subject:   strconv.Itoa(int(user.Id)),
		Issuer:    user.FirstName + " " + user.LastName,
		IssuedAt:  time.Now().Unix(),
	}

	// create token
	token, err := jwt.NewWithClaims(jwt.SigningMethodHS256, payload).SignedString([]byte("secret"))

	if err != nil {
		return c.Status(400).JSON(fiber.Map{
			"message": "invalid credentials",
		})
	}

	//set COOKIES
	cookie := fiber.Cookie{
		Name:     "jwt",
		Value:    token,
		Expires:  time.Now().Add(time.Hour * 24),
		HTTPOnly: true,
	}

	c.Cookie(&cookie)

	return c.JSON(fiber.Map{
		"message": "success",
	})
}

// ** User : get logged in user
func IsAuthenticatedUser(c *fiber.Ctx) error {
	id, _ := middlewares.GetUserId(c)

	var user models.User
	database.DB.Where("id = ?", id).First(&user)

	return c.JSON(user)
}

// ** Logout function ** \\
func Logout(c *fiber.Ctx) error {
	cookie := fiber.Cookie{
		Name:     "jwt",
		Value:    "",
		Expires:  time.Now().Add(-1 * time.Hour),
		HTTPOnly: true,
	}
	c.Cookie(&cookie)

	return c.JSON(fiber.Map{
		"message": "success",
	})
}

// ** UpdateUser function ** \\
func UpdateInfo(c *fiber.Ctx) error {
	var data map[string]string

	if err := c.BodyParser(&data); err != nil {
		return err
	}

	id, err := middlewares.GetUserId(c)

	if err != nil {
		return c.Status(401).JSON(fiber.Map{
			"message": "Unauthorized",
		})
	}

	user := models.User{

		FirstName: data["first_name"],
		LastName:  data["last_name"],
		Email:     data["email"],
	}
	user.Id = uint(id)
	database.DB.Model(&user).Updates(&user)
	return c.JSON(user)
}

// ** UpdatePassword function ** \\
func UpdatePassword(c *fiber.Ctx) error {
	var data map[string]string

	if err := c.BodyParser(&data); err != nil {
		return err
	}
	if data["password"] == "" {
		return c.Status(400).JSON(fiber.Map{
			"message": "Password cannot be empty",
		})
	}
	// compare password and confirm password if not equal return error
	if data["password"] != data["password_confirm"] {
		return c.Status(400).JSON(fiber.Map{
			"message": "Password and password confirmation do not match",
		})
	}

	id, err := middlewares.GetUserId(c)

	if err != nil {
		return c.Status(401).JSON(fiber.Map{
			"message": "Unauthorized",
		})
	}

	user := models.User{}
	user.Id = uint(id)
	user.Password = user.SetPassword(data["password"])

	database.DB.Model(&user).Updates(&user)
	return c.JSON(fiber.Map{
		"message": "successfully updated password",
	})
}

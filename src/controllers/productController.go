package controllers

import (
	"strconv"

	"github.com/gofiber/fiber/v2"
	"www.lama.com/src/database"
	"www.lama.com/src/models"
)

func Products(c *fiber.Ctx) error {
	var products []models.Product

	database.DB.Find(&products)
	return c.JSON(products)
}

func CreateProducts(c *fiber.Ctx) error {
	var product models.Product

	if err := c.BodyParser(&product); err != nil {
		return c.Status(503).SendString("Error parsing body")
	}

	database.DB.Create(&product)
	return c.JSON(product)
}

// ** GET /product/:id ** \\
func GetProductById(c *fiber.Ctx) error {
	var product models.Product

	id, err := strconv.Atoi(c.Params("id"))
	if err != nil {
		return c.Status(400).JSON(fiber.Map{
			"message": "invalid id",
		})
	}
	// check if product exists
	database.DB.Find(&product, id)
	if product.Id == 0 {
		return c.Status(400).JSON(fiber.Map{
			"message": "No such product found",
		})
	}

	product.Id = uint(id)

	database.DB.First(&product)
	return c.JSON(product)
}

func UpdateProduct(c *fiber.Ctx) error {
	// updating product
	var product models.Product
	id, err := strconv.Atoi(c.Params("id"))
	if err != nil {
		return c.Status(400).SendString("No such Products")
	}
	product.Id = uint(id)
	database.DB.Find(&product)
	if err := c.BodyParser(&product); err != nil {
		return c.Status(503).SendString("No such product found")
	}
	database.DB.Model(&product).Updates(&product)
	return c.JSON(product)
}

func DeleteProduct(c *fiber.Ctx) error {
	var product models.Product
	id, err := strconv.Atoi(c.Params("id"))
	if err != nil {
		return c.Status(400).SendString("No such Products")
	}
	product.Id = uint(id)
	database.DB.Find(&product)
	database.DB.Delete(&product)
	return c.JSON("Product deleted")
}

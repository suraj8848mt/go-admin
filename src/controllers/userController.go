package controllers

import (
	"github.com/gofiber/fiber/v2"
	"www.lama.com/src/database"
	"www.lama.com/src/models"
)

func Ambassadors(c *fiber.Ctx) error {
	var users []models.User
	database.DB.Where("is_ambassador = ?", true).Find(&users)

	return c.JSON(users)
}

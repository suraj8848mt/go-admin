package controllers

import (
	"strconv"

	"github.com/gofiber/fiber/v2"
	"www.lama.com/src/database"
	"www.lama.com/src/models"
)

func Link(c *fiber.Ctx) error {
	id, err := strconv.Atoi(c.Params("id"))
	if err != nil {
		return c.Status(400).JSON(fiber.Map{
			"message": "invalid id",
		})
	}
	var links []models.Link

	// check if links exists
	database.DB.Where("id = ?", id).Find(&links)
	//check length of links and return  empty array if length is 0
	if len(links) == 0 {
		return c.JSON(links)
	}

	for _, link := range links {
		var orders []models.Order

		database.DB.Where("code = ? and complete= true", link.Code).Find(&orders)
		orders = append(orders, link.Orders)

	}

	return c.JSON(links)
}

package main

import (
	"github.com/bxcodec/faker/v3"
	"www.lama.com/src/database"
	"www.lama.com/src/models"
)

func main() {
	database.Connect()
	for i := 0; i < 30; i++ {
		ambassador := models.User{
			FirstName:    faker.FirstName(),
			LastName:     faker.LastName(),
			Email:        faker.Email(),
			IsAmbassador: true,
		}
		ambassador.SetPassword("123456")

		database.DB.Create(&ambassador)
	}
}

package main

import (
	"math/rand"

	"github.com/bxcodec/faker/v3"
	"www.lama.com/src/database"
	"www.lama.com/src/models"
)

func main() {
	database.Connect()
	// populate products
	for i := 0; i < 30; i++ {
		product := models.Product{
			Model:       models.Model{},
			Title:       faker.Name(),
			Description: faker.Sentence(),
			Image:       faker.URL(),
			Price:       float64(rand.Intn(100) + 10),
		}
		database.DB.Create(&product)
	}
}

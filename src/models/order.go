package models

type Order struct {
	Model
	TransactionId   string      `gorm:"type:varchar(255);null" json:"transaction_id"`
	UserId          uint        `gorm:"type:int(11);null" json:"user_id"`
	Code            string      `gorm:"type:varchar(255);null" json:"code"`
	AmbassadorEmail string      `gorm:"type:varchar(255);null" json:"ambassador_email"`
	FirstName       string      `gorm:"type:varchar(255);null" json:"first_name"`
	LastName        string      `gorm:"type:varchar(255);null" json:"last_name"`
	Name            string      `gorm:"-" json:"name"`
	Email           string      `gorm:"type:varchar(255);null" json:"email"`
	Address         string      `gorm:"type:varchar(255);null" json:"address"`
	City            string      `gorm:"type:varchar(255);null" json:"city"`
	Country         string      `gorm:"type:varchar(255);null" json:"country"`
	Zip             string      `gorm:"type:varchar(255);null" json:"zip"`
	Complete        bool        `gorm:"default:false" json:"-"`
	Total           float64     `gorm:"-" json:"total"`
	OrderItems      []OrderItem `gorm:"foreignkey:OrderId" json:"order_item"`
}

type OrderItem struct {
	Model
	OrderId           uint    `gorm:"type:int(11);null" json:"order_id"`
	ProductTitle      string  `gorm:"type:varchar(255);null" json:"product_title"`
	ProductPrice      float64 `gorm:"type:float(11);null" json:"product_price"`
	Quantity          uint    `gorm:"type:int(11);null" json:"quantity"`
	AdminRevenue      float64 `gorm:"type:float(11);null" json:"admin_revenue"`
	AmbassadorReveneu float64 `gorm:"type:float(11);null" json:"ambassador_revenue"`
}

// ** GET FULL NAME ** \\
func (order *Order) FullName() string {
	return order.FirstName + " " + order.LastName
}

// ** GET TOTAL ** \\
func (order *Order) GetTotal() float64 {
	var total float64 = 0
	for _, orderItem := range order.OrderItems {
		total += orderItem.ProductPrice * float64(orderItem.Quantity)
	}
	return total
}

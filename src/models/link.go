package models

type Link struct {
	Model
	Code     string    `gorm:"type:varchar(255);not null" json:"code"`
	UserId   uint      `gorm:"type:int;not null" json:"user_id"`
	User     User      `gorm:"foreignkey:UserId" json:"user"`
	Products []Product `gorm:"many2many:link_products" json:"products"`
	Orders   Order     `gorm:"-" json:"orders"`
}

package models

import (
	"golang.org/x/crypto/bcrypt"
	"www.lama.com/src/validator"
)

type User struct {
	Model
	FirstName    string `gorm:"type:varchar(255)" json:"first_name"`
	LastName     string `gorm:"type:varchar(255)" json:"last_name"`
	Email        string `gorm:"type:varchar(255)" json:"email"`
	Password     []byte `gorm:"type:varchar(255)" json:"-"`
	IsAmbassador bool   `gorm:"type:boolean" json:"-"`
}

//function to validate user input
func ValidateUser(v *validator.Validator, user *User) {
	v.Check(user.FirstName != "", "first_name", "required")
	v.Check(user.LastName != "", "last_name", "required")
	v.Check(user.Email != "", "email", "required")
	v.Check(user.Password != nil, "password", "required")
	v.Check(validator.IsEmail(user.Email), "email", "invalid email.")
}

// method to create bcrypt hash of password
func (user *User) SetPassword(password string) []byte {
	hashedPassword, _ := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	return hashedPassword
}

// method to compare password with bcrypt hash
func (user *User) ComparePassword(password string) error {
	return bcrypt.CompareHashAndPassword(user.Password, []byte(password))
}

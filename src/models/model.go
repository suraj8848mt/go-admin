package models

type Model struct {
	Id uint `gorm:"primary_key" json:"id"`
}

package models

type Product struct {
	Model
	Title       string  `gorm:"type:varchar(255);not null" json:"title"`
	Description string  `gorm:"type:varchar(255);not null" json:"description"`
	Image       string  `gorm:"type:varchar(255);not null" json:"image"`
	Price       float64 `gorm:"type:decimal(10,2);not null" json:"price"`
}

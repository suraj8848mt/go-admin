package validator

import "regexp"

var (
	// ErrValidation is the default error for validation
	EmailRx = regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")
)

// Define a new Validator type which contains a map of validation errors.
type Validator struct {
	Errors map[string]string
}

// New is a helper which creates a new Validator instance with an empty errors map.
func New() *Validator {
	return &Validator{Errors: make(map[string]string)}
}

// Valid returns true if the errors map doesn't contain any entries.
func (v *Validator) Valid() bool {
	return len(v.Errors) == 0
}

// AddError adds an error message to the map (so long as no entry already exists for
// the given key).
func (v *Validator) AddError(key string, message string) {
	if _, exists := v.Errors[key]; !exists {
		v.Errors[key] = message
	}
}

// Check adds an error message to the map only if a validation check is not 'ok'.
func (v *Validator) Check(ok bool, key, message string) {
	if !ok {
		v.AddError(key, message)
	}
}

// In returns true if a specific value is in a list of strings
func In(value string, list ...string) bool {
	for i := range list {
		if value == list[i] {
			return true
		}
	}
	return false
}

// Matches returns true if a string matches a regular expression
func Matches(value string, rx *regexp.Regexp) bool {
	return rx.MatchString(value)
}

// MinLength returns true if a string is at least a certain length
func MinLength(s string, n int) bool {
	return len(s) >= n
}

// MaxLength returns true if a string is at most a certain length
func MaxLength(s string, n int) bool {
	return len(s) <= n
}

// LengthBetween returns true if a string is between a certain length
func LengthBetween(s string, min, max int) bool {
	return len(s) >= min && len(s) <= max
}

// IsEmail returns true if a string is a valid email address
func IsEmail(s string) bool {
	return EmailRx.MatchString(s)
}

// IsURL returns true if a string is a valid url
func IsURL(s string) bool {
	return Matches(s, regexp.MustCompile(`^https?://`))
}

// IsIP returns true if a string is a valid IP address
func IsIP(s string) bool {
	return Matches(s, regexp.MustCompile(`^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$`))
}

// IsMAC returns true if a string is a valid MAC address
func IsMAC(s string) bool {
	return Matches(s, regexp.MustCompile(`^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$`))
}

// IsCIDR returns true if a string is a valid CIDR address
func IsCIDR(s string) bool {
	return Matches(s, regexp.MustCompile(`^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(\/([0-9]|[1-2][0-9]|3[0-2]))$`))
}

// Unique returns true if all string values in a slice are unique.
func Unique(values []string) bool {
	uniqueValues := make(map[string]bool)

	for _, value := range values {
		uniqueValues[value] = true
	}
	return len(values) == len(uniqueValues)
}

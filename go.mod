module www.lama.com

go 1.17

require gorm.io/gorm v1.23.2

require (
	github.com/bxcodec/faker/v3 v3.8.0 // indirect
	github.com/cosmtrek/air v1.29.0 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/fatih/color v1.13.0 // indirect
	github.com/fsnotify/fsnotify v1.5.1 // indirect
	github.com/go-playground/locales v0.14.0 // indirect
	github.com/go-playground/universal-translator v0.18.0 // indirect
	github.com/go-playground/validator/v10 v10.10.1 // indirect
	github.com/go-sql-driver/mysql v1.6.0 // indirect
	github.com/gofiber/utils v0.0.10 // indirect
	github.com/golang-jwt/jwt v3.2.2+incompatible // indirect
	github.com/gorilla/schema v1.1.0 // indirect
	github.com/imdario/mergo v0.3.12 // indirect
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/mattn/go-colorable v0.1.12 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/pelletier/go-toml v1.9.4 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	golang.org/x/text v0.3.7 // indirect
)

require (
	github.com/andybalholm/brotli v1.0.4 // indirect
	github.com/gofiber/fiber v1.14.6
	github.com/gofiber/fiber/v2 v2.29.0 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.4 // indirect
	github.com/klauspost/compress v1.15.0 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasthttp v1.34.0 // indirect
	github.com/valyala/tcplisten v1.0.0 // indirect
	golang.org/x/crypto v0.0.0-20220314234724-5d542ad81a58
	golang.org/x/sys v0.0.0-20220310020820-b874c991c1a5 // indirect
	gorm.io/driver/mysql v1.3.2
)

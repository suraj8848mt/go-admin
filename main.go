package main

import (
	"flag"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"www.lama.com/src/database"
	"www.lama.com/src/routes"
)

// version of application
const version = "0.0.1"

type config struct {
	port int
	env  string
	db   struct {
		dsn          string
		maxOpenConns int
		maxIdleConns int
		maxIdleTime  string
	}
}

func main() {

	// declare an instance of config struct
	var cfg config
	//Read the value of port and env from command line
	flag.IntVar(&cfg.port, "port", 8000, "Port to listen on")
	flag.StringVar(&cfg.env, "env", "development", "Environment(development|staging|production)")

	//Database connection
	database.Connect()

	// Database migrations
	database.AutoMigrate()

	// App setup
	app := fiber.New(fiber.Config{
		Prefork: true,
	})

	app.Use(cors.New(cors.Config{
		AllowCredentials: true,
	}))

	// Routes setup
	routes.Setup(app)

	app.Listen(":8000")
	app.Server().MaxConnsPerIP = 1
}

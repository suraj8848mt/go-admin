FROM golang:1.17.4


RUN addgroup admin && adduser --ingroup admin --disabled-password --gecos '' admin
WORKDIR /app

COPY go.mod .
COPY go.sum .
RUN go mod download




COPY . .
# binary will be $(go env GOPATH)/bin/air
RUN curl -sSfL https://raw.githubusercontent.com/cosmtrek/air/master/install.sh | sh -s -- -b $(go env GOPATH)/bin

RUN chown -R admin:admin /app

USER admin
CMD ["air"]